<p><b>This page is no longer maintained. Currently, only KDE 4.2.0
and newer are maintained. Please have a look at the
<a href="4.4.0.php">KDE 4.4.0 Info Page</a> instead.</b></p>
