
KDE Security Advisory: kpdf/xpdf multiple integer overflows
Original Release Date: 2005-12-07
URL: http://www.kde.org/info/security/advisory-20051207-1.txt

0. References
        CAN-2005-3191
        CAN-2005-3192
        CAN-2005-3193


1. Systems affected:

        KDE 3.2.0 up to including KDE 3.5.0
	KOffice 1.3.0 up to including KOffice 1.4.2


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        multiple integer overflow vulnerabilities that allow specially
	crafted pdf files, when opened, to overflow a heap allocated
	buffer and execute arbitrary code. 


3. Impact:

        Remotely supplied pdf files can be used to execute arbitrary
	code on the client machine.


4. Solution:

	The patches that were circulating after the initial public disclosure
	and which were backported to kpdf were incomplete. We're preparing
	updates and they will be announced and published under

        http://www.kde.org/info/security/advisory-20051207-2.txt
