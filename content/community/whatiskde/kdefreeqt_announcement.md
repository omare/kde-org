---
title: "KDE Free Qt Foundation"
hidden: true
---

This is the original announcement about the creation of the KDE Free Qt
Foundation from June 1998.

## Announcement

The KDE project and Troll Tech AS, the creators of Qt, are pleased
to announce the founding of the 'KDE Free Qt Foundation'.

The purpose of this foundation is to guarantee the availability of
Qt for free software development now and in the future.

The foundation will control the rights to the Qt Free Edition and
ensure that current and future releases of Qt will be available for
free software development at all times. All changes to the Qt Free
Edition license will have to be approved by the KDE Free Qt Foundation
which will consist of two members of Troll Tech AS as well as
two members of the KDE project. One of the representatives of the
KDE project will have a double vote to be used in case of a tie.

Should Troll Tech ever discontinue the Qt Free Edition for any rea-
son including, but not limited to, a buy-out of Troll Tech, a merger
or bankruptcy, the latest version of the Qt Free Edition will be
released under the BSD license.

Furthermore, should Troll Tech cease continued development of Qt, as
assessed by a majority of the KDE Free Qt Foundation, and not
release a new version at least every 12 months, the Foundation has
the right to release the Qt Free Edition under the BSD License.

At this point lawyers are working on the details of the agreement.
Troll Tech and the KDE project expect to be able to sign the
necessary documents within a few weeks.

We believe the founding of the KDE Free Qt Foundation to be an
unprecedented ground-breaking step, ushering in a new era of software
development, allowing the KDE project, the free software community,
all free software developers as well as commercial software
developers to prosper in a mutually supportive fashion.


<p style="display: table-cell; padding: 1ex;">
Bernd Johannes Wuebben <br />
The KDE Project<br />
&#119;ue&#x62;ben&#x40;&#107;&#00100;&#x65;.&#111;&#x72;&#0103;
</p>
<p style="display: table-cell; padding: 1ex;">Eirik Eng<br />
Troll Tech CEO<br />
Eirik.Eng@troll.no<br />
</p>

