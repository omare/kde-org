---
title: KDE Applications Improve Usability, Performance and Take You to Mars
date: "2013-02-06"
hidden: true
---

## Less intrusive Notifications in Kate

Kate—KDE's Advanced Text Editor—received improvements in many areas. Thanks to an intensive <a href='http://ev.kde.org'>KDE e.V.</a>-sponsored <a href='http://dot.kde.org/2012/11/24/katekdevelop-october-sprint-whats-new-kate'>coding sprint in October</a>, Kate got an improved notification system, an optional 'minimap' as scrollbar, a new Project Management plugin, predefined color schemes, improvements to the scripting interface and much more. A great bugfixing effort reduced the number of open bug reports from 850 to 60. Outside of the coding sprint, work was done to create a new Quick Open functionality and other enhancements. All these improvements also benefit applications using Kate Part for text editing, including the lightweight KWrite text editor and KDevelop, the complete KDE Integrated Development Environment (IDE).

{{< figure src="/announcements/announce-4.10/kate-notifications.png" caption="Kate's new passive notifications are less disruptive to your workflow"width="600px" >}}

## Konsole Enhancements

Konsole brings back print-screen and send signals functionality formerly present in KDE 3, as well as options to change line spacing and the requirement of CTRL key when drag'n'dropping text, new support for xterm's 1006 mouse extension and the ability to clear the command line before using bookmarks for some commands.

{{< figure src="/announcements/announce-4.10/konsole-top.png" caption="New functions have entered Konsole"width="600px" >}}

## Tiled Rendering Improves Okular Performance

Okular, KDE's universal document viewer, gains improvements.The newest features include a technique called <a href='http://tsdgeos.blogspot.com/2012/11/okular-tiled-rendering-merged-to-master.html'>tiled rendering</a> which allows Okular to zoom in further and faster while reducing memory consumption compared to previous versions. The embedded video feature has been improved. Editing and creating annotations in Okular has become more user-friendly with the introduction of high precision QTabletEvents. Now a tablet behaves exactly like a mouse except when creating an annotation. With this task, the high precision position of the QTabletEvent is used, so free-hand annotations are smoother. A new feature allows easy history navigation, which can now be accessed by forward and back mouse buttons. Okular Active, the touch-friendly version of the powerful document reader is now part of KDE's core applications.
It is Plasma Active's new Ebook Reader and has been optimized for reading documents on a touch device.
Okular Active supports a wide range of Ebook file formats.

{{< figure src="/announcements/announce-4.10/okular-tiled-rendering.png" caption="Tiled rendering in Okular: faster zooming using less memory"width="600px" >}}

## Gwenview Gets Activity Support

Gwenview, KDE's image viewer, features improved thumbnail handling and generation as well as Activity support. It supports color correction of JPG and PNG files, working with KWin to adjust to the color profiles of different monitors, allowing for consistent color representation of photos and graphics. The Gwenview image importer now works recursively, showing all images available for display below, as well as within, a specified folder. For more details, see <a href='http://agateau.com/2012/12/04/changes-in-gwenview-for-kde-sc-4.10'>the blog of Gwenview's maintainer, Aurélien Gateau</a>.

{{< figure src="/announcements/announce-4.10/gwenview.png" caption="Gwenview, KDE's versatile image viewer"width="600px" >}}

Kopete, KDE's chat application, gained a real &quot;Do Not Disturb&quot; mode which disables visual and sound notifications entirely.

## Kontact Improves Performance

KDE's PIM applications have gotten many bugfixes and improvements. Substantial work with the search backend has vastly improved email indexing and retrieval, delivering more responsive applications with lower resource usage.

{{< figure src="/announcements/announce-4.10/kontact.png" caption="Kontact Groupware Client"width="600px" >}}

KMail has a new ability to automatically resize images attached to emails, configurable in KMail's settings. KMail also introduces Text Autocorrection, including word replacement and capitalize the first letter of a sentence. The settings and word lists are shared with Calligra Words and are configurable. HTML composer support has been expanded: tables can be inserted, with control over rows and columns as well as the ability to merge cells. Defined sizes for images are now also supported, as is the ability to insert html code directly . The 'plain text' companion to HTML emails was also improved, with HTML tags convertible to plain text equivalents. Other improvements to KMail include: opening recent files in the composer, adding new Contacts directly from KMail and attaching vcards to emails.

The import wizard gained support for importing settings from Opera, settings and data from Claws Mail and Balsa, and tags from Thunderbird and Claws Mail.

{{< figure src="/announcements/announce-4.10/kontact-mail.png" caption="Kontact Mail Client"width="600px" >}}

## Major Improvements in Gaming Apps

KDE Games and Educational applications have seen widespread changes. KDE Games benefited from major improvements to basic libraries, resulting in smoother gameplay. A new and improved KTouch typing tutor debuts and Marble continues to improve, securing its place as the premier world viewing and mapping tool in the Free Software world.

A new game is included with this release. <a href='http://games.kde.org/game.php?game=picmi'>Picmi</a> is a single player logic-based puzzle game. The object of the game is to color cells according to numbers given at the side of the board in order to complete a hidden pattern or picture. Picmi includes two game modes—random puzzles are generated according to the selected difficulty settings or the included preset puzzles.

{{< figure src="/announcements/announce-4.10/picmi.png" caption="Picmi, a new game in KDE Applications 4.10"width="600px" >}}

## Printing Sudoku Puzzles

Other KDE Games and educational applications have been improved, including the ability to print puzzles from KSudoku so they can be used away from the computer. KGoldrunner was rewritten based on the new KDEGames libraries; gameplay and UI are the same, but the game is prettier and smoother. KJumpingCube now allows adjusting the speed of moves and animates multi-stage moves to make them easier to understand. The UI has been improved and you can now choose which one you'd like to play against: Kepler or Newton. Smaller boards offer simplified playing styles. KAlgebra has some improvements to the interface and Pairs gained a theme editor.

{{< figure src="/announcements/announce-4.10/ksudoku.png" caption="Print puzzles from KSudoku for your offline use"width="600px" >}}

For the Rocs Graph Theory IDE, developers overhauled the user interface and configuration dialog to make it easier to use. They also introduced support for TGF, DOT/Graphvis (import/export) and TikZ/PGF (export only) files.

## To Moon and Mars: New Space Orbiter visualization for Marble in KDE 4.10

The <a href='http://marble.kde.org/'>Marble Virtual Globe</a> makes further strides into the area of Space Science. René Küttner, a student from <a href='http://tu-dresden.de/en'>TU Dresden</a> (Dresden University of Technology), worked on Marble as part of the <a href='http://sophia.estec.esa.int/socis2012/'>ESA SoCiS 2012 program</a>. The Summer of Code in Space was carried out for the second time by the <a href='http://www.esa.int/ESA'>European Space Agency</a> and again Marble was chosen as a mentoring organization.

{{< figure src="/announcements/announce-4.10/marble-indonesia.png" caption="Marble Desktop Globe"width="600px" >}}

René developed a visualization of space orbiters around other planets inside the Marble Virtual Globe. As a result, Marble can display the positions and orbit tracks of space missions such as <a href='http://www.esa.int/Our_Activities/Space_Science/Mars_Express'>Mars Express</a>, <a href='http://www.esa.int/Our_Activities/Space_Science/Venus_Express'>Venus Express</a> and <a href='http://www.esa.int/Our_Activities/Space_Science/SMART-1'>SMART-1</a>. The visualization also includes the positions of the two Mars moons Phobos and Deimos. He also enhanced Marble's display of Earth satellite tracks. A <a href='http://www.youtube.com/watch?v=K_VA0XtvjYk'>video</a> presents some of the features that have been added during this program. Thank you to <a href='http://www.esa.int/ESA'>ESA</a> and the <a href='http://sophia.estec.esa.int/socis2012/?q=sponsors'>SoCiS Sponsors</a> for funding this project.

{{< figure src="/announcements/announce-4.10/marble-mars.png" caption="Marble on Mars"width="600px" >}}

The <a href='http://marble.kde.org/changelog.php#v15'>Marble visual changelog</a> has more information on this project and other Marble news.

## Enjoy Practicing Touch-Typing

Ktouch, KDE's touch typing tutor has been rewritten. It now features a clean, elegant and vibrant user interface to make learning and practicing touch typing as enjoyable as it can be. <a href='http://blog.sebasgo.net/blog/2012/11/28/november-update-for-ktouch/'>The new user interface</a> reduces complexity, and guides the user with color cues and inobtrusive animations. Many new features help improve the overall training experience—a new course editor has built-in quality checks, the user can review progress and identify weaknesses, the overall appearance is attractive and scaled to screen size, hinting and obvious problem solving tips are displayed prominently.

{{< figure src="/announcements/announce-4.10/ktouch.png" caption="KTouch Touch Typing Tutor"width="600px" >}}

#### Installing KDE Applications

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

{{% i18n_var "KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/%[1]s'>http://download.kde.org</a> and can also be obtained on <a href='http://www.kde.org/download/cdrom.php'>CD-ROM</a> or with any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today." "4.10.0" %}}

##### Packages

{{% i18n_var "Some Linux/UNIX OS vendors have kindly provided binary packages of %[1]s for some versions of their distribution, and in other cases community volunteers have done so." "4.10.0"%}}

##### Package Locations

{{% i18n_var "For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_%[1]s'>Community Wiki</a>." "4.10.0"%}}

{{% i18n_var "The complete source code for %[1]s may be <a href='/info/%[1]s.php'>freely downloaded</a>. Instructions on compiling and installing KDE software %[1]s are available from the <a href='/info/%[1]s.php#binary'>%[1]s Info Page</a>." "4.10.0"%}}

#### System Requirements

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Also Announced Today:

## Plasma Workspaces 4.10 Improve Mobile Device Support and Receive Visual Refinement</a>

Several components of Plasma Workspaces have been ported to Qt Quick/QML framework. Stability and usability have been improved. A new print manager and Color Management support have been introduced.

## KDE Platform 4.10 Opens Up More APIs to Qt Quick</a>

This release makes it easier to contribute to KDE with a Plasma SDK (Software Development Kit), the ability to write Plasma widgets and widget collections in the Qt Markup Language (QML), changes in the libKDEGames library, and new scripting capabilities in window manager KWin.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
