---
title: "KDE Ships Plasma 5.5.4, bugfix Release for January"
description: "KDE Ships Plasma 5.5.4"
date: 2016-01-26
release: plasma-5.5.4
layout: plasma
changelog: plasma-5.5.3-5.5.4-changelog
---

{{<figure src="/announcements/plasma-5.5/plasma-5.5.png" alt="Plasma 5.5 " class="text-center" width="600px" caption="Plasma 5.5">}}

Tuesday, 26 January 2016.

Today KDE releases a bugfix update to Plasma 5, versioned 5.5.4. <a href='https://www.kde.org/announcements/plasma-5.5.0.php'>Plasma 5.5</a> was released in December with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Many improvements and refactoring to notification positioning making them appear in the right place for multi-screen use.
