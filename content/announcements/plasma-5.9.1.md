---
title: KDE Plasma 5.9.1, Bugfix Release
release: plasma-5.9.1
description: KDE Ships Plasma 5.9.1.
date: 2017-02-14
layout: plasma
changelog: plasma-5.9.0-5.9.1-changelog
---

{{%youtube id="lm0sqqVcotA"%}}

{{<figure src="/announcements/plasma-5.9/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center" width="600px" caption="KDE Plasma 5.9">}}

Tuesday, 7 February 2017.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.9.1. <a href='https://www.kde.org/announcements/plasma-5.9.0.php'>Plasma 5.9</a> was released in January with many feature refinements and new modules to complete the desktop experience.

This release adds a week"s worth of new translations and fixes from KDE"s contributors. The bugfixes are typically small but important and include:

- Fix i18n extraction: xgettext doesn't recognize single quotes. <a href="https://commits.kde.org/plasma-desktop/8c174b9c1e0b1b1be141eb9280ca260886f0e2cb">Commit.</a>
- Set wallpaper type in SDDM config. <a href="https://commits.kde.org/sddm-kcm/19e83b28161783d570bde2ced692a8b5f2236693">Commit.</a> Fixes bug <a href="https://bugs.kde.org/370521">#370521</a>
