---
title: Plasma 5.17.2 Complete Changelog
version: 5.17.2
hidden: true
plasma: true
type: fulllog
---

### <a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a>

- [GTK3] Revert checkbox recolouring. <a href='https://commits.kde.org/breeze-gtk/b1649126c8c6d1af746a8472939fcda40a260095'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412078'>#412078</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24994'>D24994</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Notifier: make it possible to replace the instance. <a href='https://commits.kde.org/discover/4c13b6c371cf35c11befad0ffd6f1826e2f7ff02'>Commit.</a>
- App delegate: improve on narrow windows. <a href='https://commits.kde.org/discover/badd210475a402539130b28ad413f0ccde3ad374'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411828'>#411828</a>
- Flatpak: oops. <a href='https://commits.kde.org/discover/eea4e757cea70e28b80a580ba9ac8850aca5429c'>Commit.</a>
- Pk: notify about problems regarding file listing. <a href='https://commits.kde.org/discover/72a68025e7bfaee5c456b884fa0f3d4d30f95433'>Commit.</a> See bug <a href='https://bugs.kde.org/412986'>#412986</a>
- Appstream: support more formats of appstream urls. <a href='https://commits.kde.org/discover/4f2aa69241717a12e09bd49aba9ff78bd202960a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408419'>#408419</a>
- Notifier: don't autostart outside of Plasma. <a href='https://commits.kde.org/discover/6bf3c252e329e2a84fbef19daaab7ced9892466b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413235'>#413235</a>
- Snap: fix cancelling. <a href='https://commits.kde.org/discover/31c9dac67093e5c7e46c7af124105d4ebfeec064'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404358'>#404358</a>
- Pk: readability. <a href='https://commits.kde.org/discover/a0ed1cc67025c06626d394fcb691d49213e9063d'>Commit.</a>
- Pk: Make action buttons translatable. <a href='https://commits.kde.org/discover/98a5716c4de98b08e34691a75d5b6ba3c10a667c'>Commit.</a>
- Notifier: Make action buttons translatable. <a href='https://commits.kde.org/discover/d877aa09ba17b1d8615d06b5b09fa22e1be5b0d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24894'>D24894</a>
- Pk: don't show redundant packages on updates. <a href='https://commits.kde.org/discover/54428ea0ab22a5d239a1053384db8210137f83aa'>Commit.</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a>

- Typo--. <a href='https://commits.kde.org/drkonqi/45ea4d554c540e0b84c6c84245c0350bcdac7e91'>Commit.</a>

### <a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a>

- Create directory when saving the menu file. <a href='https://commits.kde.org/kmenuedit/d7dbd84e96b8727a8f37abb8744b2dcf27ff5fb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413079'>#413079</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24996'>D24996</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Dmabuf recovery on EGL reset. <a href='https://commits.kde.org/kwin/7459aabcac2471862a35a7c045de7176b5435f1c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411980'>#411980</a>. See bug <a href='https://bugs.kde.org/413403'>#413403</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24954'>D24954</a>
- [kcmkwin/kwindecoration] Fix default window size in KCMShell. <a href='https://commits.kde.org/kwin/a151edd29b0095450a3714fb246dde7a8516a55c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413557'>#413557</a>
- [kcmkwin/desktop] Elide "Show animation when switching" checkbox text. <a href='https://commits.kde.org/kwin/105976a42a25e25410e9e1373cd2f6da4c5d8655'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403151'>#403151</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24988'>D24988</a>
- [kcmkwin/kwinvirtualdesktops] Improve default window size when opened in kcmshell. <a href='https://commits.kde.org/kwin/ee2507129be16e4b027b816dc19e2d944657d5e2'>Commit.</a>
- [scripting] Provide conversion functions for AbstractClient. <a href='https://commits.kde.org/kwin/a738ecce85c14b7cc4e7d1f3c1c01c3db1c9c0e7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413044'>#413044</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24944'>D24944</a>
- Don't use MESA_EGL_NO_X11_HEADERS. <a href='https://commits.kde.org/kwin/8e176c8b0d97e78e8b33fac3b5acded277bbcf4f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24840'>D24840</a>
- [kcmkwin/kwindecoration] Elide "theme default border size" CheckBox. <a href='https://commits.kde.org/kwin/2ce1d99221118ead24f5e5076fa06816144cd2ce'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24885'>D24885</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Fix force font DPI UI logic. <a href='https://commits.kde.org/plasma-desktop/e37f9dba5b776080ff5a388ddc4f3e111541bf49'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25004'>D25004</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [wallpapers/image] Randomise new batches of images in the slideshow. <a href='https://commits.kde.org/plasma-workspace/9dca7d6cd44cbb16c6d7fb1aca5588760544b1d6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413463'>#413463</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24986'>D24986</a>
- [wallpapers/image] Seed random number generator. <a href='https://commits.kde.org/plasma-workspace/868a6a9f62546be0c409615501bf54fb6fa8c910'>Commit.</a> See bug <a href='https://bugs.kde.org/413463'>#413463</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24985'>D24985</a>
- [Lock Screen] Don't use black shadows with black text. <a href='https://commits.kde.org/plasma-workspace/d029fb058cff2be4ebbae6ccd891c1a4577144e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413537'>#413537</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24997'>D24997</a>
- Clear the cells before relayouting the items. <a href='https://commits.kde.org/plasma-workspace/0e25e9ad108d074f01b3a70a77e1fb7899b05058'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413019'>#413019</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- [sidebar] Add a hover effect to intro page icons. <a href='https://commits.kde.org/systemsettings/a306b76cb8531905b463e33e52b1176c0073d4f1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24901'>D24901</a>
