---
title: KDE Plasma 5.8.9, Bugfix Release for February
release: plasma-5.8.9
version: 5.8.9
description: KDE Ships 5.8.9
date: 2018-02-07
changelog: plasma-5.8.8-5.8.9-changelog
layout: plasma
---

{{%youtube id="LgH1Clgr-uE"%}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8 " class="text-center" width="600px" caption="KDE Plasma 5.8">}}

Wednesday, 7 February 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.8.9." %}}

{{% i18n_var `<a href="https://www.kde.org/announcements/plasma-%[1]s.0.php">Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience.` "5.8" "October" %}}

This release adds three months worth of new translations and fixes from KDE"s contributors. The bugfixes are typically small but important and include:

* Make sure device paths are quoted. <a href="https://commits.kde.org/plasma-workspace/9db872df82c258315c6ebad800af59e81ffb9212">Commit.</a> Fixes bug <a href="https://bugs.kde.org/389815">#389815</a>
* Sanitise notification HTML. <a href="https://commits.kde.org/plasma-workspace/5bc696b5abcdb460c1017592e80b2d7f6ed3107c">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D10188">D10188</a>
* Fixed a freeze caused by certain notifications. <a href="https://commits.kde.org/plasma-workspace/5e230a6290b1ff61e54c43da48821eb2bf3192ae">Commit.</a> Fixes bug <a href="https://bugs.kde.org/381154">#381154</a>
* Fix for xembedsniproxy crash due to NULL returned from xcb_image_get(). <a href="https://commits.kde.org/plasma-workspace/12e3568042fb365aad3eccf2fefa58bbeb065210">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D9732">D9732</a>
