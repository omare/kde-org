---
title: KDE Plasma 5.14.5, Bugfix Release for January
release: "plasma-5.14.5"
version: "5.14.5"
description: KDE Ships Plasma 5.14.5.
date: 2019-01-08
layout: plasma
changelog: plasma-5.14.4-5.14.5-changelo
---

{{<figure src="/announcements/plasma-5.14/plasma-5.14.png" alt="Plasma 5.14" class="text-center" width="600px" caption="KDE Plasma 5.14">}}

Tuesday, 8 January 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.14.5." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in October with many feature refinements and new modules to complete the desktop experience." "5.14" %}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- [weather dataengine] Updates to bbc, envcan and noaa weather sources.
- KDE Plasma Addons Comic Plasmoid: several fixes to make updates more reliable.
- Make accessibility warning dialog usable again and fix event handling. <a href="https://commits.kde.org/plasma-desktop/36e724f1f458d9ee17bb7a66f620ab7378e5e05e">Commit.</a> Phabricator Code review <a href="https://  phabricator.kde.org/D17536">D17536</a>
