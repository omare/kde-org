---
title: "KDE 3.1.4 to 3.1.5 Changelog"
hidden: true
---

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.1.4 and 3.1.5 releases.
</p>
<p>
Please see the <a href="changelog3_1_3to3_1_4.php">3.1.3 to 3.1.4 changelog</a> for further information.
</p>

<h3>arts</h3><ul>
</ul>

<h3>kdelibs</h3><ul>
<li>networking: Fix crash related to SOCKS</li>
<li>kate: C syntax highlighting</li>
<li>konqueror: Improved handling of long filenames in icon view.</li>
<li>khtml: Minor rendering fixes</li>
<li>khtml: Improved loading of large images</li>
<li>java: Crash fix</li>
<li>webdav: Fixed renaming of files via secure connection</li>
</ul>

<h3>kdeaddons</h3><ul>
</ul>

<h3>kdeadmin</h3><ul>
<li>kuser: Fix quota support for linux quota 3.10 or newer (e.g. SuSE 9)</li>
</ul>

<h3>kdeartwork</h3><ul>
</ul>

<h3>kdebase</h3><ul>
<li>konqueror: Fixed "Location bar forgets what you type if you dont press enter and change tabs"</li>
<li>konqueror: Fixed "tabs resize (shrink) when clicked on while loading"</li>
<li>konqueror: Don't show hardcoded shortcuts in tab context menu</li>
<li>konqueror: Made "Detach Tab" create new konqueror window of proper size</li>
<li>konqueror: Set location bar URL for new tabs immediately</li>
<li>konsole: Fixed "List Sessions" feature.</li>
</ul>

<h3>kdebindings</h3><ul>
</ul>

<h3>kdeedu</h3><ul>
<li>kstars: Fixed Daylight Savings Time rule for locations in Saskatchewan, 
Canada (these locations do not use DST).</li>
<li>kstars: Improved determination of the time that an object sets or 
transits.  If the transit or set time is before the object's rise time, 
then it is recomputed for the following day.  These times appear in the 
popup menu, and in the details dialog.</li>
</ul>

<h3>kdegames</h3><ul>
<li>atlantik: Don't interrupt token movement when player leaves jail on doubles</li>
<li>atlantik: Fixes in libatlantikui, prevents some crashes with Atlantik Designer</li>
<li>atlantik: Two valgrind fixes preventing crashes</li>
</ul>

<h3>kdegraphics</h3><ul>
<li>kiconedit: Fix loading of remote files</li>
</ul>

<h3>kdemultimedia</h3><ul>
</ul>

<h3>kdenetwork</h3><ul>
<li>kmail: Fixed problem with external filters dropping last character of mail</li>
<li>kmail: Detect when another KMail instance is already running</li>
<li>kaddressbook: Fix problem with import of Netscape addressbooks</li>
</ul>

<h3>kdepim</h3><ul>
<li>kalarm: Fix system tray status indication on startup</li>
</ul>

<h3>kdesdk</h3><ul>
<li>kbabel: Fixed freeze during spellcheck</li>
<li>kompare: Several fixes</li>
</ul>

<h3>kdetoys</h3><ul>
</ul>

<h3>kdeutils</h3><ul>
<li>kdf: Fixed mem leak</li>
<li>khexedit: Fixed 64 bit float big endian decoding</li>
<li>khexedit: Honour ignore case setting in find next (F3)</li>
<li>khexedit: Fix crash on systems which need strict alignments for double variables (HP-UX)</li>
</ul>

<h3>quanta</h3><ul>
</ul>
