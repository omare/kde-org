---
title: "KDE 4.1.4 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.1.4</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_1_4/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
     <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix corruption of URLs with Chinese characters. See SVN commit <a href="http://websvn.kde.org/?rev=906060&amp;view=rev">906060</a>. </li>
        <li class="bugfix ">Allow desktop files that define both an application and a library, like cervisia.desktop, for compatibility. Better separate them for the future though. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=178603">178603</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=906293&amp;view=rev">906293</a>. </li>
      </ul>
      </div>
     <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix item not disappearing from the filemanager when renaming to a name that starts with a dot (hidden file). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=164974">164974</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=889593&amp;view=rev">889593</a>. </li>
        <li class="bugfix crash">Fix infinite recursion and crash when the slot connected to newItems calls openUrl, like ktorrent's scanfolder plugin does. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174920">174920</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=889717&amp;view=rev">889717</a>. </li>
        <li class="bugfix crash">Fix crash in the "rename dialog" when resuming an aborted download. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=135395">135395</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=889786&amp;view=rev">889786</a>. </li>
        <li class="bugfix ">Fix toggling of hidden files on and off. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174788">174788</a>. </li>
        <li class="bugfix ">Fix drag-n-drop from desktop icons into KDE3 applications. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=175910">175910</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=179921">179921</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890187&amp;view=rev">890187</a>. </li>
        <li class="bugfix crash">Fix crash when typing '*' in a file manager's treeview. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=176555">176555</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=891650&amp;view=rev">891650</a>. </li>
      </ul>
      </div>
     <h4><a name="kfile">kfile</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Don't reload the current directory when selecting a file in the file dialog (which could even crash if the file got deleted externally). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=173454">173454</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890379&amp;view=rev">890379</a>. </li>
      </ul>
      </div>
     <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Backport API additions so that the webarchiver plugin from extragear can compile against kdelibs-4.1.4. See SVN commit <a href="http://websvn.kde.org/?rev=906618&amp;view=rev">906618</a>. </li>
        <li class="bugfix ">Many other bugfixes, too many to be listed here.</li>
      </ul>
      </div>
     <h4><a name="kbuildsycoca">kbuildsycoca</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix changes in filetype-application associations (adding or removing applications) not working (it would appear like it wasn't saved). Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=164584">164584</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=178560">178560</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=901205&amp;view=rev">901205</a>. </li>
        <li class="bugfix ">Fix for user-created desktop files (for instance by typing a command in the "open with" dialog) not being usable on some distributions (depending on the vfolder setup). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=178561">178561</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=901693&amp;view=rev">901693</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_1_4/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="plasma">plasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Amarok support in the Now Playing data engine. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174044">174044</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=879159&amp;view=rev">879159</a>. </li>
      </ul>
      </div>
      <h4><a name="kioexec">kioexec</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix kioexec not asking to upload anymore, after making changes to a remote file with a non-kio-enabled application. See SVN commit <a href="http://websvn.kde.org/?rev=882355&amp;view=rev">882355</a>. </li>
      </ul>
      </div>
      <h4><a name="drkonqi">drkonqi</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Many improvements in the automatic cleaning up of backtraces, to remove "(no debugging symbols found)" lines for instance. See SVN commit <a href="http://websvn.kde.org/?rev=889778&amp;view=rev">889778</a>. </li>
      </ul>
      </div>
      <h4><a name="libkonq (used by dolphin and konqueror)">libkonq (used by dolphin and konqueror)</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix duplicate entries in the recent folder list of the Copy To / Move To submenus. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=177858">177858</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=897799&amp;view=rev">897799</a>. </li>
      </ul>
      </div>
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix error message from kfind when doing "Find" on remote directories. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169473">169473</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=899045&amp;view=rev">899045</a>. </li>
      </ul>
      </div>
      <h4><a name="filetypes">filetypes</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when adding an application and the list shows "None" translated. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=168934">168934</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=900427&amp;view=rev">900427</a>. </li>
        <li class="bugfix ">Prevent nspluginscan from generating mimetypes with a leading newline character. The actual bug is in mplayerplug-in.so though, which doesn't implement NP_GetMIMEDescription properly. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=178562">178562</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=901203&amp;view=rev">901203</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_1_4/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">VIM-like HJKL navigation in the content view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174647">174647</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890790&amp;view=rev">890790</a>. </li>
      </ul>
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Printing improvements, including duplex printing and margins. See SVN commit <a href="http://websvn.kde.org/?rev=896270&amp;view=rev">896270</a>. </li>
          <li class="improvement">Print to File for PDF, DVI, and DjVu documents. See SVN commit <a href="http://websvn.kde.org/?rev=897005&amp;view=rev">897005</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Comicbook backend: corrently identify CBR documents with .cbz extension, and the other way round. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174701">174701</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=882007&amp;view=rev">882007</a> and <a href="http://websvn.kde.org/?rev=898355&amp;view=rev">898355</a>. </li>
        <li class="bugfix ">PDF backend: make sure to print with the correct paper size. See SVN commit <a href="http://websvn.kde.org/?rev=885277&amp;view=rev">885277</a>. </li>
        <li class="bugfix ">TIFF backend: scale down an image if it does fit into its page. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174447">174447</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890789&amp;view=rev">890789</a>. </li>
        <li class="bugfix ">Fix text search not find occurrences in 1-page documents. See SVN commit <a href="http://websvn.kde.org/?rev=890801&amp;view=rev">890801</a>. </li>
        <li class="bugfix ">Hopefully fix the endless scrollbar resizing loop. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160628">160628</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=894428&amp;view=rev">894428</a>. </li>
        <li class="bugfix ">Correctly toggle the "Show Menubar" menu action at startup. See SVN commits <a href="http://websvn.kde.org/?rev=894124&amp;view=rev">894124</a> and <a href="http://websvn.kde.org/?rev=898743&amp;view=rev">898743</a>. </li>
        <li class="bugfix ">Postscript backend: support gz- and bz2- compressed .eps documents. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=177516">177516</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=896221&amp;view=rev">896221</a>. </li>
        <li class="bugfix ">Fix annotations moving for documents with a non-zero orientation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=177598">177598</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=898293&amp;view=rev">898293</a>. </li>
        <li class="bugfix ">Comicbook backend: recursively scan for images in CBZ archives. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=178029">178029</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=899137&amp;view=rev">899137</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="modulehomepage"> [ <a href="http://pim.kde.org/">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_4/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Distinguish disabled from enabled alarm colour when highlighted in alarm list. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=177798">177798</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=897759&amp;view=rev">897759</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix toolbar settings being lost. See SVN commit <a href="http://websvn.kde.org/?rev=892193&amp;view=rev">892193</a>. </li>
        <li class="bugfix ">Ensure alarm windows show on top of full-screen windows. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=168962">168962</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=902298&amp;view=rev">902298</a>. </li>
        <li class="bugfix ">Fix failure to update alarms in KOrganizer when Kontact is running but Kontact's calendar component is not loaded. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=176759">176759</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=893626&amp;view=rev">893626</a> and <a href="http://websvn.kde.org/?rev=895458&amp;view=rev">895458</a>. </li>
        <li class="bugfix ">Fix invalid alarm remaining in calendar when pre-alarm action failure message is acknowledged before the alarm is deferred. See SVN commit <a href="http://websvn.kde.org/?rev=906260&amp;view=rev">906260</a>. </li>
        <li class="bugfix ">Fix click on system tray icon not showing main window if 'Show in system tray' configuration setting deselected. See SVN commit <a href="http://websvn.kde.org/?rev=897380&amp;view=rev">897380</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kontact.kde.org/kmail" name="kmail">KMail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Correctly recoginze the mimetype of attachments sent with KDE3. See SVN commit <a href="http://websvn.kde.org/?rev=880534&amp;view=rev">880534</a>. </li>
        <li class="bugfix ">Fix potential mail loss when rebuilding the disconnected IMAP cache. See SVN commit <a href="http://websvn.kde.org/?rev=883016&amp;view=rev">883016</a>. </li>
        <li class="bugfix ">Probably fix the problem with duplicated mails when using online IMAP with spam filtering. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=95064">95064</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=883868&amp;view=rev">883868</a>. </li>
        <li class="bugfix ">Don't show shortcuts for filters and folders in the shortcut dialog, since editing the shortcuts there doesn't work. See SVN commit <a href="http://websvn.kde.org/?rev=883869&amp;view=rev">883869</a>. </li>
        <li class="bugfix ">Make adding subfolders in the folder selector dialog work again. See SVN commit <a href="http://websvn.kde.org/?rev=883871&amp;view=rev">883871</a>. </li>
        <li class="bugfix ">Unify the look between receiving and sending accounts a bit. See SVN commits <a href="http://websvn.kde.org/?rev=883874&amp;view=rev">883874</a> and <a href="http://websvn.kde.org/?rev=883876&amp;view=rev">883876</a>. </li>
        <li class="bugfix ">Fix mail loss when using POP3 with spam filtering and the /tmp partition gets full. See SVN commit <a href="http://websvn.kde.org/?rev=884326&amp;view=rev">884326</a>. </li>
        <li class="bugfix ">Don't show the add to addressbook button when a contact is already in the addressbook. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174332">174332</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890865&amp;view=rev">890865</a>. </li>
        <li class="bugfix ">Fix crash when the template quote indicator does not end with an underscore. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174268">174268</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890868&amp;view=rev">890868</a>. </li>
        <li class="bugfix ">Don't use deprecated spamassassin flags anymore. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=140032">140032</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890872&amp;view=rev">890872</a>. </li>
        <li class="bugfix ">Fix possible crash when parsing the procmail file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=173964">173964</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890876&amp;view=rev">890876</a>. </li>
        <li class="bugfix ">Fix several encoding issues when inserting or attaching a file. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=88781">88781</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=64815">64815</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=890879&amp;view=rev">890879</a>, <a href="http://websvn.kde.org/?rev=890882&amp;view=rev">890882</a> and <a href="http://websvn.kde.org/?rev=890890&amp;view=rev">890890</a>. </li>
        <li class="bugfix ">Make the questionbox that appears when not all characters in the composer are supported by the current encoding work correctly. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=149309">149309</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=145163">145163</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890881&amp;view=rev">890881</a>. </li>
        <li class="bugfix ">Fix a crash when calling a D-Bus method with an incorrect parameter. See SVN commit <a href="http://websvn.kde.org/?rev=890891&amp;view=rev">890891</a>. </li>
        <li class="bugfix ">Make the open in addressbook button work again. See SVN commit <a href="http://websvn.kde.org/?rev=893933&amp;view=rev">893933</a>. </li>
        <li class="bugfix ">Fix a crash when using filters with online IMAP. See SVN commit <a href="http://websvn.kde.org/?rev=895059&amp;view=rev">895059</a>. </li>
        <li class="bugfix ">Fix the GPG key selector dialog not finishing the search for the keys. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169563">169563</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=895194&amp;view=rev">895194</a>. </li>
        <li class="bugfix ">Fix crash when finishing the account wizard. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174992">174992</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890863&amp;view=rev">890863</a>. </li>
        <li class="bugfix ">Fix crashing in templates when using the DNL code in certain conditions. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=178038">178038</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=899969&amp;view=rev">899969</a>. </li>
        <li class="bugfix ">Correctly apply the forward template when inline-forwarding messages with attachments. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=178128">178128</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=146921">146921</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=899972&amp;view=rev">899972</a> and <a href="http://websvn.kde.org/?rev=900344&amp;view=rev">900344</a>. </li>
        <li class="bugfix ">Don't allow attachment deletion/editing in read-only folders. See SVN commit <a href="http://websvn.kde.org/?rev=899974&amp;view=rev">899974</a>. </li>
        <li class="bugfix ">Fix crash when clicking on one of the invitation action links. See SVN commit <a href="http://websvn.kde.org/?rev=899975&amp;view=rev">899975</a>. </li>
        <li class="bugfix ">Fix a crash with disconnected IMAP when syncing flags from a server while we have a search folder that has a search rule which needs the complete body. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=106030">106030</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=904948&amp;view=rev">904948</a>. </li>
        <li class="bugfix ">Also detect messages with X-Priority == 1 as urgent. See SVN commit <a href="http://websvn.kde.org/?rev=904951&amp;view=rev">904951</a>. </li>
        <li class="bugfix ">Don't freeze when editing an outgoing account in certain situations. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170728">170728</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=905542&amp;view=rev">905542</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kontact.kde.org" name="kontact">Kontact</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix the display of the number of unread mails in the summary. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174304">174304</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=883877&amp;view=rev">883877</a>. </li>
        <li class="bugfix ">Fix crash when opening the settings dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174707">174707</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=890871&amp;view=rev">890871</a>. </li>
        <li class="bugfix ">Don't create duplicate calendar resources in certain circumstances. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=140041">140041</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=904955&amp;view=rev">904955</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="modulehomepage"> [ <a href="http://utils.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_4/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not delete text in editor if passphrase dialog is canceled. See SVN commit <a href="http://websvn.kde.org/?rev=884986&amp;view=rev">884986</a>. </li>
        <li class="bugfix ">Don't ask to save an empty text in editor. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=173405">173405</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=891312&amp;view=rev">891312</a>. </li>
        <li class="bugfix ">Save sort column and order of keymanager window. See SVN commit <a href="http://websvn.kde.org/?rev=892292&amp;view=rev">892292</a>. </li>
        <li class="bugfix ">Make search the default action in keyserver dialog instead of close. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=179322">179322</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=906027&amp;view=rev">906027</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="modulehomepage"> [ <a href="http://edu.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_4/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/kstars" name="kstars">KStars</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Make tab order in Location Dialog more intuitive See SVN commit <a href="http://websvn.kde.org/?rev=906243&amp;view=rev">906243</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix localization issues with FOV size in the FOV Editor See SVN commits <a href="http://websvn.kde.org/?rev=906161&amp;view=rev">906161</a> and <a href="http://websvn.kde.org/?rev=906170&amp;view=rev">906170</a>. </li>
        <li class="bugfix ">Localize Information link / Image link titles in the Detail dialog See SVN commit <a href="http://websvn.kde.org/?rev=906173&amp;view=rev">906173</a>. </li>
      </ul>
      </div>
    </div>
