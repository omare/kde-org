2007-06-21 12:06 +0000 [r678439]  binner

	* branches/KDE/3.5/kdewebdev/kimagemapeditor/kimagemapeditor.desktop,
	  branches/KDE/3.5/kdewebdev/kommander/editor/kmdr-editor.desktop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.desktop: fix invalid
	  .desktop files

2007-06-25 13:13 +0000 [r680119]  dfaure

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/debugXSL.cpp:
	  Fix typo for coolo BUG: 147200

2007-07-03 17:47 +0000 [r682934]  aacid

	* branches/KDE/3.5/kdewebdev/kimagemapeditor/kimedialogs.cpp:
	  having a 2000 there was really a bad bad bad idea, kde photo
	  group is much wider :D

2007-09-29 20:38 +0000 [r718834]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/executor/dcopkommanderif.h,
	  branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/executor/instance.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/function.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parsenode.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parserdata.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderwidget.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parserdata.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/connectioneditorimpl.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parser.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/tabwidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/executor/main.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/executor/register.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/pluginmanager/pluginmanager.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parser.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/specials.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/kommander-editor.kdevelop,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderfunctions.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/listbox.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/specials.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/assoctexteditorimpl.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/functionlib.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/factory/kommanderversion.h,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/kommanderplugin.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/assoctexteditorimpl.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/mainwindow.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/executor/instance.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parsenode.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderwidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/VERSION,
	  branches/KDE/3.5/kdewebdev/kommander/kommander.kdevelop: Merge
	  some changes from the Kommander 1.3.0 working branch created by
	  Michal in 2006... Summary of changes: o function str_findrev
	  works now o fix crash when diving by zero o make function names
	  case insensitive o fix dcop() function (external DCOP) in new
	  parser o add missing EOLs to array_values() o don't open files
	  with no filename given o fix crash on incorrect syntax in new
	  parser o fix input_file() function o add switch/case to new
	  parser o fix importing tables with empty cells

2007-09-29 20:55 +0000 [r718841]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/widgets/radiobutton.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/scriptobject.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/closebutton.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/specialinformation.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parserdata.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/dialog.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/richtexteditor.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/konsole.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/statusbar.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/tabwidget.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/slider.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/listbox.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/timer.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/specials.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/buttongroup.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/radiobutton.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/textbrowser.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/progressbar.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/spinboxint.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/closebutton.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/function.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/statusbar.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parser.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/subdialog.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/tabwidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/combobox.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/timer.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/specials.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/expression.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/execbutton.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/fileselector.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/textbrowser.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/lineedit.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/myprocess.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/function.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/pixmaplabel.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderwidget.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/groupbox.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/wizard.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/combobox.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/expression.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/subdialog.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parser.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/label.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/fileselector.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/execbutton.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/pluginmanager/pluginmanager.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderfunctions.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/functionlib.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/scriptobject.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/checkbox.h,
	  branches/KDE/3.5/kdewebdev/kommander/executor/instance.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/pixmaplabel.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderwidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/dialog.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/wizard.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/richtexteditor.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/lineedit.h,
	  branches/KDE/3.5/kdewebdev/kommander/factory/kommanderfactory.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/myprocess.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/groupbox.h,
	  branches/KDE/3.5/kdewebdev/kommander/pluginmanager/pluginmanager.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/executor/main.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/konsole.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/label.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/checkbox.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/slider.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/progressbar.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/listbox.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parsenode.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/spinboxint.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/buttongroup.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/plugin.cpp: More
	  things merged from the work branch, mostly styling changes and
	  QString::null->QString() usage.

2007-09-29 21:02 +0000 [r718845]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/editor/kommanderui.rc
	  (added): Add from branch.

2007-09-29 21:56 +0000 [r718864]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/editor/mainwindow.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/pics/small/shellscript.png
	  (added),
	  branches/KDE/3.5/kdewebdev/kommander/editor/pics/Makefile.am,
	  branches/KDE/3.5/kdewebdev/kommander/editor/pics/images.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/widgetdatabase.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/editor/pics/mkimages,
	  branches/KDE/3.5/kdewebdev/kommander/editor/mainwindowactions.cpp:
	  Make the splash work, add icon for Run Dialog onto the toolbar.

2007-10-03 10:26 +0000 [r720608]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/rescanprj.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix recursive
	  symlink handling. BUG: 145651

2007-10-03 11:29 +0000 [r720634]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix add to project
	  when saving a new file into a symlinked directory BUG: 148529

2007-10-03 13:05 +0000 [r720675]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/components/csseditor/stylesheetparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/components/csseditor/cssselector.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Do not lose CSS
	  selectors after editing inside the dialog if they are repeated.
	  Note: preview for the further instance of the same selector will
	  most probably not work correctly. BUG: 145413

2007-10-04 07:44 +0000 [r720983]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/node.h,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/components/tableeditor/tableeditor.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/undoredo.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: This should finally
	  fix all the cases when Quanta crashes due to double free in the
	  node tree. It's more a workaround, but that's what I have now...

2007-10-04 09:05 +0000 [r720995]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Make the parser as
	  fast as it was in 3.5.6 and before, but keep the double deletion
	  checks.

2007-10-04 09:46 +0000 [r721001]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: Fix
	  help button in the New Project wizard. BUG: 145324

2007-10-04 15:38 +0000 [r721099]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/tag.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/sagroupparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Do not add the same
	  entry over and over to the completion list. Fix --enable-final.

2007-10-04 16:07 +0000 [r721111-721109]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp: This should
	  fix the latest crash reported in parser. If not, reopen it. BUG:
	  141057

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp: Add
	  comment.

2007-10-06 14:55 +0000 [r722062]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/kommander-editor.kdevelop,
	  branches/KDE/3.5/kdewebdev/kommander/widget/parser.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/kommander.kdevelop: Make
	  multiplying work correctly for floating point values.

2007-10-06 15:29 +0000 [r722069]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/editor/assoctexteditorimpl.cpp:
	  Do not remove the whole text after undoing the changes in the
	  editor. BUG: 147817

2007-10-06 15:46 +0000 [r722079]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/editor/formfile.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/editor/formfile.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/mainwindowactions.cpp:
	  Don't crash if opening a file the second time when the first try
	  failed. BUG: 141110

2007-10-06 16:11 +0000 [r722089]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/widgets/dialog.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/dialog.h: Don't
	  close the dialogs with ESC (just like in real applications). BUG:
	  123071

2007-10-06 16:42 +0000 [r722103]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/execbutton.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderwidget.cpp:
	  Fix ExecButton.setEnabled(). BUG: 113624

2007-10-06 16:48 +0000 [r722109]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/fileselector.cpp:
	  Use icon instead of "..." in the file selector BUG: 109985

2007-10-06 18:24 +0000 [r722145]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widget/kommanderfunctions.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/functionlib.cpp: Make
	  @echo really work. BUG: 138705

2007-10-06 18:39 +0000 [r722153]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/executor/dcopkommanderif.h,
	  branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/executor/instance.h,
	  branches/KDE/3.5/kdewebdev/kommander/executor/instance.cpp: Add
	  DCOP method to get the winID. BUG: 111161

2007-10-06 18:48 +0000 [r722156-722155]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/executor/dcopkommanderif.h,
	  branches/KDE/3.5/kdewebdev/kommander/executor/instance.h,
	  branches/KDE/3.5/kdewebdev/kommander/executor/instance.cpp: Add
	  DCOP method to change the cursor to the wait cursor and back.
	  BUG: 109630

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog: Update changelog.

2007-10-06 21:20 +0000 [r722195]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog: indentation
	  change

2007-10-08 11:06 +0000 [r722975]  coolo

	* branches/KDE/3.5/kdewebdev/kdewebdev.lsm: updating lsm

