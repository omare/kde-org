---
title: "KDE 4.8.3 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.8.3</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_8_3/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Remove extraneous libpng library call, which will be unsupported at libpng-1.6.0 or soon after. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=296305">296305</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/a5cc5111d6dd640e63779e0bb46ca75dee80f714">a5cc511</a>. </li>
        <li class="bugfix normal">Fix rendering of css padding in text input form elements. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=280445">280445</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=280226">280226</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/dee2801c0a86fec1c642da9710b17b0a7bec3ecd">dee2801</a>. </li>
        <li class="bugfix crash">Fixed a crash that could occur when oxygen animations are enabled. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=249453">249453</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=297967">297967</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=297901">297901</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=285158">285158</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=271466">271466</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/6f8d94f5c2ff8078cfcfcc1f9ce6d23d815cd86c">6f8d94f</a>. </li>
        <li class="bugfix normal">AdBlock: update/load filter lists only when AdBlock is enabled. See Git commit <a href="http://commits.kde.org/kdelibs/abc933ccee0af644bc642dc7551651d1ce0ca29a">abc933c</a>. </li>
        <li class="bugfix normal">AdBlock: check if an error page is received when updating filter lists from remote server. See Git commit <a href="http://commits.kde.org/kdelibs/532d7c16c1f02de2cee028cd9bce7b7614c2737d">532d7c1</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_8_3/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://dolphin.kde.org" name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix current item selection issues. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=297488">297488</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=298782">298782</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/68ce395a192362969783615e50a8004d3029eb7e">68ce395</a>. </li>
        <li class="bugfix normal">When Escape is pressed, clear the selection and cancel the keyboard search. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=298742">298742</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/d3b1312d3edc615b03fb17c1a5fa62ccb61cdabd">d3b1312</a>. </li>
        <li class="bugfix normal">Revert the 2.0 decision to always use KB for file-sizes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=289850">289850</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/5559aaaa0bf2f731ce2413f140b20f4a2d3c60b3">5559aaa</a>. </li>
        <li class="bugfix normal">Fix drag and drop cursor issue. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293850">293850</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/dafcc815b8f604b95e57986d3c3288b02f8d20d2">dafcc81</a>. </li>
        <li class="bugfix normal">Fix filtering issue. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=297269">297269</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/ed5f3aaba7149b3816909a403bbd9f3e06d1a774">ed5f3aa</a>. </li>
      </ul>
      </div>
      <h4><a name="konsole">konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Konsole does not supprt selecting text with shift + arrow keys in mcedit Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=59256">59256</a>.  See Git commit <a href="http://commits.kde.org/konsole/b74cc7f2446cdd709ec293505f55360e0fabc7b5">b74cc7f</a>. </li>
        <li class="bugfix normal">hidden question dialog prevents application quit via taskbar Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=163677">163677</a>.  See Git commit <a href="http://commits.kde.org/konsole/66c0ddf7d1d5c9a7009f33da418d27a186651b92">66c0ddf</a>. </li>
        <li class="bugfix normal">Konsole blanks on tab re-order Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=164099">164099</a>.  See Git commit <a href="http://commits.kde.org/konsole/ccfc3f859c5695cc08895570efd0831db0d3b9b0">ccfc3f8</a>. </li>
        <li class="bugfix minor">The command prompt isn't displayed after using "Clear Scrollback &amp; reset" Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=250212">250212</a>.  See Git commit <a href="http://commits.kde.org/konsole/ccfc3f859c5695cc08895570efd0831db0d3b9b0">ccfc3f8</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_8_3/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when searching. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=297379">297379</a>.  See Git commit <a href="http://commits.kde.org/okular/c29ce712cf699e38a3a384fc69e5bf36cd26fd8e">c29ce71</a>. </li>
        <li class="bugfix crash">Fix scrolling from the thumbnail view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=298362">298362</a>.  See Git commit <a href="http://commits.kde.org/okular/25f27760905b0266303bbbffabd5e241873f62f6">25f2776</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_8_3/kdeworkspace.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
        <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">KWin option to display window geometry when moving or resizing does not work with desktop effects disabled Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292839">292839</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/8c1dfb2250a94b60549d9665ff4550170da92d1d">8c1dfb2</a>. </li>
        <li class="bugfix normal">Long caption in Thumbnail layout overlaps box for only one item Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=297028">297028</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/41b0647ad4ad052a3b6b049cb8ffdc526a6e03fd">41b0647</a>. </li>
        <li class="bugfix normal">Thumbnail not placed in center of TabBox when there is only one window Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=297856">297856</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/f9a5f5d74bc5be5d0614eeb2c11db56a3e44a377">f9a5f5d</a>. </li>
        <li class="bugfix normal">Compilation with GLES fails if OpenGL is not available Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=298143">298143</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/1554ba408633397066d31c3d565c217b5a385fb8">1554ba4</a>. </li>
        <li class="bugfix crash">MouseMark does not implement xrender backend nor ::supported() Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=298338">298338</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/2e25ded28b574d8dce9a98a6504a259f7f86aa6a">2e25ded</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_8_3/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kmail">KMail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Remove BCC when attaching messages. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=297934">297934</a>.  See Git commit <a href="http://commits.kde.org/kdepim/46648213ff39174e2629ef594b2b1e241d19d665">4664821</a>. </li>
        <li class="bugfix normal">Fix conflict when we doubleclick on a message. Change status to read twice, it conflicted. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=298083">298083</a>.  See Git commit <a href="http://commits.kde.org/kdepim/0cb710fa5941b3f2aa0b618358aedb7f6911fd11">0cb710f</a>. </li>
        <li class="bugfix normal">The key sequence 'N' was ambiguous in kmail. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=220667">220667</a>.  See Git commit <a href="http://commits.kde.org/kdepim/0fee16b9baeb89329e4e41d626a68e2caa31b5b7">0fee16b</a>. </li>
        <li class="bugfix normal">'Reply to' used wrong identity even if we explicitly set in 'folder properties'. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=297912">297912</a>.  See Git commit <a href="http://commits.kde.org/kdepim/d4da0039e83bd2e33b74fc08b5b4681155f1698d">d4da003</a>. </li>
        <li class="bugfix normal">Add folder path in title in message dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=298181">298181</a>.  See Git commit <a href="http://commits.kde.org/kdepim/939dc5a83703774e77aee429587f35eb4fae58eb">939dc5a</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_8_3/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix generation of DSA/ElGamal keys with GnuPG 1.x. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=253408">253408</a>.  See Git commit <a href="http://commits.kde.org/kgpg/654048def1ca9d89d31076e4d19ae0ed6d1d06f0">654048d</a>. </li>
        <li class="bugfix normal">Use selected key size for both primary and subkey length. See Git commit <a href="http://commits.kde.org/kgpg/fff63e22bf32dfe6e771e188f44c1a9695a40418">fff63e2</a>. </li>
      </ul>
      </div>
    </div>
