------------------------------------------------------------------------
r1057336 | weilbach | 2009-12-02 01:31:26 +0000 (Wed, 02 Dec 2009) | 2 lines

Backport of async dbus call patch, which stops hanging the plasma-overlay.

------------------------------------------------------------------------
r1059582 | mmrozowski | 2009-12-07 00:37:31 +0000 (Mon, 07 Dec 2009) | 1 line

[applets/frame/CMakeLists.txt] Backport #1059579 - Remove duplicated macro_display_feature_log call.
------------------------------------------------------------------------
r1060851 | scripty | 2009-12-10 04:17:37 +0000 (Thu, 10 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1062188 | scripty | 2009-12-14 04:20:10 +0000 (Mon, 14 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1064498 | scripty | 2009-12-21 04:23:49 +0000 (Mon, 21 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1069551 | lunakl | 2010-01-03 18:19:57 +0000 (Sun, 03 Jan 2010) | 5 lines

backport r1069550:
Use KColorUtils::lighten/darken instead of Color::ligher/darker - the latter
for whatever reason turns #ffc0c0 to some kind of grey.


------------------------------------------------------------------------
r1072388 | scripty | 2010-01-10 04:14:09 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1074986 | scripty | 2010-01-15 04:03:34 +0000 (Fri, 15 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
