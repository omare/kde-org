------------------------------------------------------------------------
r1184505 | annma | 2010-10-11 04:57:31 +1300 (Mon, 11 Oct 2010) | 3 lines

backport of 1184499


------------------------------------------------------------------------
r1184960 | ruberg | 2010-10-12 12:50:17 +1300 (Tue, 12 Oct 2010) | 3 lines

Fix problem with tooltip. It was displayed behind the keyboard widget - but only with activated compositing and after having moved around the panel


------------------------------------------------------------------------
r1185353 | scripty | 2010-10-13 15:56:19 +1300 (Wed, 13 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1186536 | pino | 2010-10-17 09:13:09 +1300 (Sun, 17 Oct 2010) | 5 lines

use KToolInvocation::invokeBrowser() if you want to launch a web browser

BUG: 253800
FIXED-IN: 4.5.3

------------------------------------------------------------------------
r1188946 | mfuchs | 2010-10-24 01:38:10 +1300 (Sun, 24 Oct 2010) | 2 lines

Does not use cached webpages but always reloads them.
BUG:254639
------------------------------------------------------------------------
