------------------------------------------------------------------------
r1097084 | uwolfer | 2010-03-01 01:34:32 +1300 (Mon, 01 Mar 2010) | 5 lines

Backport to 4.4 branch:
SVN commit 1097083 by uwolfer:

Fix system tray icon.
CCBUG:226943
------------------------------------------------------------------------
r1098178 | scripty | 2010-03-03 15:04:47 +1300 (Wed, 03 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1098398 | lueck | 2010-03-04 05:55:12 +1300 (Thu, 04 Mar 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1099275 | mfuchs | 2010-03-06 00:18:57 +1300 (Sat, 06 Mar 2010) | 2 lines

Backport r1099273
Only check if an index is valid, nothing more.
------------------------------------------------------------------------
r1099543 | reed | 2010-03-06 07:40:07 +1300 (Sat, 06 Mar 2010) | 1 line

kppp does not build on OSX
------------------------------------------------------------------------
r1100194 | scripty | 2010-03-07 15:24:06 +1300 (Sun, 07 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1100558 | rkcosta | 2010-03-08 09:42:18 +1300 (Mon, 08 Mar 2010) | 10 lines

Backport r1100557.

Apply patch by Timo Schluessler for the history import plugin.

<br> tags are not end-of-message, but a newline.

Also do not set layout twice.

CCMAIL: timo@schluessler.org

------------------------------------------------------------------------
r1101659 | fschaefer | 2010-03-11 02:54:18 +1300 (Thu, 11 Mar 2010) | 7 lines

Video config dialog: fix content of the signal standard combobox

- update content when device or input selction change
- do not set index to the index of the selected input (was nonsense !)

Backport of r1101658

------------------------------------------------------------------------
r1101733 | fschaefer | 2010-03-11 08:27:55 +1300 (Thu, 11 Mar 2010) | 7 lines

Build system: libv4l2: fix package name, add another URL

libv4l2.so is the library we use, but macro_log_feature(...) is used to display package names.
It is impossible to install libv4l2.so alone (depends on v4l2convert.so).

Backport of r1101732

------------------------------------------------------------------------
r1101988 | pali | 2010-03-12 03:26:12 +1300 (Fri, 12 Mar 2010) | 2 lines

Fixed setting status message after kopete start and after autoaway globaly

------------------------------------------------------------------------
r1101993 | pali | 2010-03-12 03:46:45 +1300 (Fri, 12 Mar 2010) | 4 lines

Fixed function sync() in skype contact
Renamed setDisplayName() to setContactDisplayName()
Do not sync skype contact when account is offline

------------------------------------------------------------------------
r1101996 | pali | 2010-03-12 03:55:34 +1300 (Fri, 12 Mar 2010) | 2 lines

Fixed setting status message for skype protocol

------------------------------------------------------------------------
r1104501 | mfuchs | 2010-03-18 09:43:55 +1300 (Thu, 18 Mar 2010) | 3 lines

HACK: Automatically restarts KIODownload if there was an error trying to resume. Is done better in master already.

BUG:231061
------------------------------------------------------------------------
r1104700 | mfuchs | 2010-03-18 23:34:08 +1300 (Thu, 18 Mar 2010) | 1 line

Adds fileModel to transferKio, that makes it possible to add checksums etc.
------------------------------------------------------------------------
r1105400 | scripty | 2010-03-20 15:31:42 +1300 (Sat, 20 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105828 | scripty | 2010-03-22 02:22:42 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
