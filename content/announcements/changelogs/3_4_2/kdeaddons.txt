2005-06-10 15:31 +0000 [r424045]  pletourn

	* konq-plugins/webarchiver/plugin_webarchiver.cpp: Remove implicit
	  conversion BUG:107137

2005-06-10 19:53 +0000 [r424101]  pletourn

	* konq-plugins/webarchiver/webarchivecreator.cpp: Build the KURL
	  the right way

2005-06-18 12:24 +0000 [r426743]  osterfeld

	* konq-plugins/akregator/feeddetector.cpp: ignore resources without
	  type attribute patch by Eckhart Woerner

2005-06-23 11:23 +0000 [r428170]  lukas

	* konq-plugins/webarchiver/archivedialog.cpp: missing i18n()

2005-06-27 15:18 +0000 [r429401]  binner

	* kdeaddons.lsm: 3.4.2 preparations

2005-06-28 19:29 +0000 [r429760]  lanius

	* konq-plugins/akregator/feeddetector.cpp: decode imported html
	  feeds, backport #105210

