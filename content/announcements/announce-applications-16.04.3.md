---
title: KDE Ships KDE Applications 16.04.3
description: KDE Ships KDE Applications 16.04.3
date: 2016-07-12
version: 16.04.3
layout: plasma
changelog: fulllog_applications-16.04.3
---

{{% i18n_var "July 12, 2016. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-16.04.0" %}}

More than 20 recorded bugfixes include improvements to ark, cantor, kate. kdepim, umbrello, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.22" %}}
