---
title: KDE Plasma 5.7.1, bugfix Release for July
release: plasma-5.7.1
description: KDE Ships Plasma 5.7.1.
date: 2016-07-12
layout: plasma
changelog: plasma-5.7.0-5.7.1-changelog
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma-5.7/plasma-5.7.png" alt="KDE Plasma 5.7 " class="text-center" width="600px" caption="KDE Plasma 5.7">}}

Tuesday, 12 July 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.7.1. <a href='https://www.kde.org/announcements/plasma-5.7.0.php'>Plasma 5.7</a> was released in July with many feature refinements and new modules to complete the desktop experience.

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix shadow rendering calculations. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=62d09fad123d9aab5afcff0f27d109ef7c6c18ba">Commit.</a> Fixes bug <a href="https://bugs.kde.org/365097">#365097</a> 'Krunner has broken shadow / corners'
- Make the systray work with scripting shell again. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6adba6315275f4ef6ebf8879c851830c268f7a51">Commit.</a>

- Fix microphone increase/decrease volume actions. <a href="http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=9f9d980414ef6b12d24bde8cf2967b519be29524">Commit.</a>
- Fix files/folders in desktop not opening with right click context menu. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6911541421dc068cee531a5fd5f322c0db5d7492">Commit.</a> Fixes bug <a href="https://bugs.kde.org/364530">#364530</a>
