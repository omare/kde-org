---
title: "KDE Plasma 5.19.4, bugfix Release for July"
version: 5.19.4
layout: plasma
changelog: "plasma-5.19.3-5.19.4-changelog"
date: 2020-07-28
---

{{% plasma-5-19-video %}}

Tuesday, 28 July 2020.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.19.4" %}}

{{< i18n_var "<a href='https://kde.org/announcements/plasma-%[1]s.0'>Plasma %[1]s</a> was released in June 2020 with many feature refinements and new modules to complete the desktop experience." "5.19" >}}

This release adds three week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

+ Plasma Networkmanager: Make hotspot configuration dialog bigger. <a href="https://commits.kde.org/plasma-nm/42806c6c82511b705a204334cab4c52c66a9f8b9">Commit.</a>
+ Only open KCM in systemsettings if it can be displayed. <a href="https://commits.kde.org/plasma-workspace/5bc6d83ae083632fa4eef21a03b1ef59dd9a852e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/423612">#423612</a>
+ Plasma Vault: Reset password field when the user clicks Ok. <a href="https://commits.kde.org/plasma-vault/5e6a53ba55fd60ace3adbd4ca57f90a5cd44992f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/424063">#424063</a>


