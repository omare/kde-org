---
title: KDE Ships November Updates
date: "2010-11-03"
description: KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.3
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Releases November Updates
</h3>

<p align="justify">
  <strong>
KDE Community Ships November Updates
</strong>
</p>

<p align="justify">
Today, KDE has released a series of updates to the Plasma Desktop and Netbook workspaces, the KDE Applications and the KDE Platform. This update is the third in a series of monthly stabilization updates to the 4.5 series. 4.5.3 brings bugfixes and translation updates on top of KDE SC 4.5 series and is a recommended update for everyone running 4.5.2 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE SC 4 is already translated into more than 55 languages, with more to come.
</p>

<p>To download source code or packages to install go to the <a href="/info/4.5.3">4.5.3 Info Page</a>.</p>

<p>
4.5.3 brings a number of improvements:
</p>
<ul>
    <li>
    KSharedDataCache has cache invalidation bug fixed that caused stability issues when daylight saving time changed.
    </li>
    <li>
    Icon overlays in Dolphin are now positioned correctly after adjusting the zoom level.
    </li>
    <li>
    Okular, KDE's universal document viewer has seen improvements in the DjVu and XPS backends.
    </li>
</ul>
<p>
The <a href="http://www.kde.org/announcements/changelogs/changelog4_5_2to4_5_3">changelog</a> lists more, if not all improvements since 4.5.2.
</p>

<div class="text-center">
	<a href="/announcements/announce-4.5/plasma-netbook-sal.png">
	<img src="/announcements/announce-4.5/thumbs/plasma-netbook-sal.png" class="img-fluid" alt="The KDE Plasma Netbook Workspace">
	</a> <br/>
	<em>The KDE Plasma Netbook Workspace</em>
</div>
<br/>

<p align="justify">
Note that the changelog is usually incomplete. For a complete list of
changes that went into 4.5.3, you can browse the Subversion log.
4.5.3 also ships a more complete set of translations for many of the 55+ supported languages.</p>

<p>To find out more about the KDE 4.5 Workspace and Applications, please refer to the
<a href="http://www.kde.org/announcements/4.5/">KDE SC 4.5.0</a>,
<a href="http://www.kde.org/announcements/4.4/">KDE SC 4.4.0</a>,
<a href="http://www.kde.org/announcements/4.3/">KDE SC 4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">KDE SC 4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">KDE SC 4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">KDE SC 4.0.0</a> release
notes. 4.5.3 is a recommended update for everyone running KDE SC 4.5.2 or earlier versions.
<p />

<p align="justify">
KDE SC, including all its libraries and its applications, is available for free
under Open Source licenses. KDE software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.5.3/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.5.3 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.3
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.5.3#binary">4.5.3 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.3
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.5.3 may be <a
href="http://download.kde.org/stable/4.5.3/src/">freely downloaded</a>.
Instructions on compiling and installing 4.5.3
  are available from the <a href="/info/4.5.3">4.5.3 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
