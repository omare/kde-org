---
title: KDE Plasma 5.8.5, Bugfix Release for December
release: plasma-5.8.5
description: KDE Ships Plasma 5.8.5.
date: 2016-12-27
layout: plasma
changelog: plasma-5.8.4-5.8.5-changelog
---

{{% youtube id="LgH1Clgr-uE" %}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8" class="text-center mt-4" width="600px" caption="KDE Plasma 5.8">}}

Tuesday, 27 December 2016.

Today KDE releases a Bugfix update to KDE Plasma 5, versioned 5.8.5. <a href='https://www.kde.org/announcements/plasma-5.8.0.php'>Plasma 5.8</a> was released in October with many feature refinements and new modules to complete the desktop experience.

This release adds a month"s worth of new translations and fixes from KDE"s contributors. The bugfixes are typically small but important and include:

- Notice when the only screen changes. <a href="https://commits.kde.org/plasma-workspace/f7b170de9fd9c4075fee324d33212b0d69909ba4">Commit.</a> Fixes bug <a href="https://bugs.kde.org/373880">#373880</a>
- Revert 'Do not ask for root permissions when it's unnecessary', it caused problems with adding a new user. <a href="https://commits.kde.org/user-manager/f2c69db182fb20453e671359e90a3bc6de40c7b0">Commit.</a> Fixes bug <a href="https://bugs.kde.org/373276">#373276</a>
- Fix compilation with Qt 5.8. <a href="https://commits.kde.org/plasma-integration/6b405fead515df417514c9aa9bb72cfa5372d2e7">Commit.</a>
