---
title: KDE Plasma 5.9.4, bugfix Release for March
release: 'plasma-5.9.4'
version: "5.9.4"
description: KDE Ships Plasma 5.9.4.
date: 2017-03-21
layout: plasma
changelog: plasma-5.9.3-5.9.4-changelog
---

{{%youtube id="lm0sqqVcotA"%}}

{{<figure src="/announcements/plasma-5.9/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.9">}}

Tuesday, 21 March 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "bugfix" "5.9.4." %}}

{{% i18n_var `<a href="https://www.kde.org/announcements/plasma-%[1]s.0.php">Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience.` "5.9" "January" %}}

{{i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "two week's" %}}

* Discover: Fix enabling/disabling sources. <a href="https://commits.kde.org/discover/7bdaa6d2f478be5422d4ef002518f2eabb1961dc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/377327">#377327</a>
* [Kicker] Fix highlighting favorites. <a href="https://commits.kde.org/plasma-desktop/db297ab5acb93f88c238778e8682effe3032bf4f">Commit.</a> See bug <a href="https://bugs.kde.org/377652">#377652</a>. Phabricator Code review <a href="https://phabricator.kde.org/D5064">D5064</a>
* System Settings: Set the correct desktop file name when in a KDE session. <a href="https://commits.kde.org/systemsettings/f61f9d8c100fe94471b1a8f23ac905e9311b7436">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D5006">D5006</a>
