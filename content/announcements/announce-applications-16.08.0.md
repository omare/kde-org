---
title: KDE Ships KDE Applications 16.08.0
description: KDE Ships KDE Applications 16.08.0
date: 2016-08-18
version: 16.08.0
changelog: fulllog_applications-16.08.0
layout: application
---

August 18, 2016. Today, KDE introduces KDE Applications 16.08, with an impressive array of upgrades when it comes to better ease of access, the introduction of highly useful functionalities and getting rid of some minor issues, bringing KDE Applications one step closer to offering you the perfect setup for your device.

{{% i18n_var "<a href='%[1]s'>Kolourpaint</a>, <a href='%[2]s'>Cervisia</a> and KDiskFree have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release." "https://www.kde.org/applications/graphics/kolourpaint/" "https://www.kde.org/applications/development/cervisia/" %}}

In the continued effort to split Kontact Suite libraries to make them easier to use for third parties, the kdepimlibs tarball has been split into akonadi-contacts, akonadi-mime and akonadi-notes.

We have discontinued the following packages: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu and mplayerthumbs. This will help us focus in the rest of the code.

### Keeping in Kontact

{{% i18n_var "<a href='%[1]s'>The Kontact Suite</a> has got the usual round of cleanups, bug fixes and optimizations in this release. Notable is the use of QtWebEngine in various compontents, which allows for a more modern HTML rendering engine to be used. We have also improved VCard4 support as well as added new plugins that can warn if some conditions are met when sending an email, e.g. verifying that you want to allow sending emails with a given identity, or checking if you are sending email as plaintext, etc." "https://userbase.kde.org/Kontact" %}}

### New Marble version

{{% i18n_var "<a href='%[1]s'>Marble</a> 2.0 is part of KDE Applications 16.08 and includes more than 450 code changes including improvements in navigation, rendering and an experimental vector rendering of OpenStreetMap data." "https://marble.kde.org/"%}}

### More Archiving

{{% i18n_var "<a href='%[1]s'>Ark</a> can now extract AppImage and .xar files as well as testing the integrity of zip, 7z and rar archives. It can also add/edit comments in rar archives" "https://www.kde.org/applications/utilities/ark/" %}}

### Terminal Improvements

{{% i18n_var "<a href='%[1]s'>Konsole</a> has had improvements regarding font rendering options and accessibility support." "https://www.kde.org/applications/system/konsole/" %}}

### And more!

{{% i18n_var "<a href='%[1]s'>Kate</a> got movable tabs. <a href='%[2]s'>More information...</a>" "https://kate-editor.org" "https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/" %}}

{{% i18n_var "<a href='%[1]s'>KGeography</a>, has added provinces and regions maps of Burkina Faso." "https://www.kde.org/applications/education/kgeography/" %}}

### Aggressive Pest Control

More than 120 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, KCalc, Kdenlive and more!

### Full Changelog

