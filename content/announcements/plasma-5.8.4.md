---
title: KDE Plasma 5.8.4, Bugfix Release for November
release: plasma-5.8.4
description: KDE Ships Plasma 5.8.4.
date: 2016-11-22
layout: plasma
changelog: plasma-5.8.3-5.8.4-changelog
---

{{%youtube id="LgH1Clgr-uE"%}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8 " class="text-center" width="600px" caption="KDE Plasma 5.8">}}

Tuesday, 22 November 2016.

Today KDE releases a Bugfix update to KDE Plasma 5, versioned 5.8.4. <a href='https://www.kde.org/announcements/plasma-5.8.0.php'>Plasma 5.8</a> was released in October with many feature refinements and new modules to complete the desktop experience.

This release adds three week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Many bug fixes for multi screen support such as:

- Load screenpool at the same time as we connect to screenchanged signals. <a href="http://commits.kde.org/plasma-workspace/7154fb681adc73c482e862febc7ad008f77058dd">Commit.</a> See bug <a href="https://bugs.kde.org/372099">#372099</a>. See bug <a href="https://bugs.kde.org/371858">#371858</a>. See bug <a href="https://bugs.kde.org/371819">#371819</a>. See bug <a href="https://bugs.kde.org/371734">#371734</a>
- Avoid connecting to screen changed signals twice. <a href="http://commits.kde.org/plasma-workspace/8a472f17ce11f3b79d740cdc21096d82b8683f3d">Commit.</a> See bug <a href="https://bugs.kde.org/372099">#372099</a>. See bug <a href="https://bugs.kde.org/371858">#371858</a>. See bug <a href="https://bugs.kde.org/371819">#371819</a>. See bug <a href="https://bugs.kde.org/371734">#371734</a>

- Make screenshots visible when there's only one screenshot too. <a href="http://commits.kde.org/discover/3297fe6026edebb8db72bb179289bee844c26ae3">Commit.</a> Fixes bug <a href="https://bugs.kde.org/371724">#371724</a>
- Disable Qt's high DPI scaling on shutdown/switch user dialogs. <a href="http://commits.kde.org/plasma-workspace/4b2abc581c6b3e7a4c2f1f893d47fad5d3806aca">Commit.</a> See bug <a href="https://bugs.kde.org/366451">#366451</a>
