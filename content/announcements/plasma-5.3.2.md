---
title: KDE Ships Plasma 5.3.2, Bugfix Release for June
description: KDE Ships Plasma 5.3.2
date: 2015-06-30
release: plasma-5.3.2
layout: plasma
changelog: plasma-5.3.1-5.3.2-changelog
---

{{<figure src="/announcements/plasma-5.3/plasma-5.3.png" alt="Plasma 5.3 " class="text-center" width="600px" caption="Plasma 5.3">}}

Tuesday, 30 June 2015.

Today KDE releases a bugfix update to Plasma 5, versioned 5.3.2. <a href='https://www.kde.org/announcements/plasma-5.3.0.php'>Plasma 5.3</a> was released in April with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- KWin: 'Defaults' should set the title bar double-click action to 'Maximize.'. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=26ee92cd678b1e070a3bdebce100e38f74c921da">Commit.</a>
- Improve Applet Alternatives dialog. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0e90ea5fea7947acaf56689df18bbdce14e8a35f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/345786">#345786</a>
- Make shutdown scripts work. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=96fdec6734087e54c5eee7c073b6328b6d602b8e">Commit.</a>
