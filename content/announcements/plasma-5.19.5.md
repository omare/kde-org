---
title: "KDE Plasma 5.19.5, bugfix Release for September"
version: 5.19.5
changelog: "plasma-5.19.4-5.19.5-changelog"
date: 2020-09-01
layout: plasma
---

{{% plasma-5-19-video %}}

Tuesday, 1 September 2020.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.19.5" >}}

{{< i18n_var `<a href='https://kde.org/announcements/plasma-%[1]s.0'>Plasma %[1]s</a> was released in June 2020 with many feature refinements and new modules to complete the desktop experience.` "5.19" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:

+ Powerdevil: On wakeup from suspend restore remembered keyboard brightness. <a href="https://commits.kde.org/powerdevil/e1ed36480cec6a49c166347efba6ab52adc0c37c">Commit.</a>
+ KSysGuard: Correctly handle monitors list changing. <a href="https://commits.kde.org/ksysguard/dbb656d54fea47fcb322ffb8e54a3b59ade316fd">Commit.</a>
+ xdg-desktop-portal-kde: enable printing of multiple copies.
