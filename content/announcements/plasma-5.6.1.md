---
title: KDE Plasma 5.6.1, Bugfix Release for April
release: "plasma-5.6.1"
description: KDE Ships Plasma 5.6.1.
date: 2016-03-29
layout: plasma
changelog: plasma-5.6.0-5.6.1-changelog
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma-5.6/plasma-5.6.png" alt="KDE Plasma 5.6 " class="mt-4 text-center" width="600px" caption="KDE Plasma 5.6">}}

Tuesday, 29 March 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.6.1. <a href='https://www.kde.org/announcements/plasma-5.6.0.php'>Plasma 5.6</a> was released in March with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix drawing QtQuickControls ComboBox popups
- Fix untranslatable string in Activities KCM.
- Show ratings in Discover Packagekit backend
